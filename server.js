const { request, response } = require('express')
const express = require('express')
const mysql = require('mysql2')
const cors = require("cors");


function openDatabaseConnection() {
  // create the connection
  const connection = mysql.createConnection({
    uri: "mysql://db:3306",
    /* mysql credentials */
    user: 'root',
    /* mysql credentials */
    password: 'root',
    /* db to be connected */
    database: 'mydb',
  })

  // open the connection
  connection.connect()

  return connection
}

const app = express()
app.use(express.json())
app.use(cors("*"));

app.get('/',(request,response)=>{
  response.send('it is workign for get request')
})
app.get('/movies', (request, response) => {
  // get the connection
  const{name} = request.params
  const connection = openDatabaseConnection()

  // prepare the statement
  const statement = `select * from Movie`

  // execute the query
  connection.query(statement, (error, data) => {
    // close the database connection
    connection.end()

    if (error) {
      // there is an error while executing the sql statement
      response.send(error)
    } else {
      // there is no error
      response.send(data)
    }
  })
})

app.post('/addmovie', (request, response) => {
  const { title, date,time,directorname } = request.body

  const statement = `
    insert into Movie
      (movie_title, movie_date,movie_time,director_name)
    values 
      ('${title}', '${date}', '${time}', '${directorname}')
  `

  const connection = openDatabaseConnection()
  connection.query(statement, (error, result) => {
    connection.end()
    response.send(result)
  })
})

app.put('/update/:title',(request,response)=>{
  const{title} = request.params
  const{directorname} = request.body

  const statement = ` update Movie
  set
    director_name='${directorname}''
  where
    movie_title = '${title}''`

    const connection = openDatabaseConnection()
  connection.query(statement, (error, result) => {
    connection.end()
    response.send(result)
  })

})

app.delete('/delete/:movie_title',(request,response)=>{
     
  const {movie_title} = request.params
  const statement = `delete from Movie where movie_title ='${movie_title}`

  const connection = openDatabaseConnection()
  connection.query(statement, (error, result) => {
    connection.end()
    response.send(result)
  })

})


app.listen(4000, "0.0.0.0",() => {
  console.log('server started on port 4000')
})
